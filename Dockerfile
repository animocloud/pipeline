FROM quay.io/buildah/stable

RUN yum update -y && yum install -y \
	git

COPY ./run-parts /usr/local/bin

RUN chmod +x /usr/local/bin/run-parts && run-parts --help	

# Set up environment variables to note that this is
# not starting with usernamespace and default to 
# isolate the filesystem with chroot.
ENV _BUILDAH_STARTED_IN_USERNS="" BUILDAH_ISOLATION=chroot

